do {
	path = File.openDialog("Choose a ND2 File to export into separate series TIFF files.");
}while(!(endsWith(path, ".nd2") || endsWith(path, ".ND2")));

print("Opening " + path);
dir = File.getParent(path);
fileName = File.getName(path);

extension = substring(fileName, lengthOf(fileName) - 4, lengthOf(fileName));

run("Bio-Formats Macro Extensions");
Ext.setId(path);
Ext.getSeriesCount(seriesCount);

print("File " + fileName + " has " + toString(seriesCount) + " series.");

setBatchMode(true);

title = fileName;
coreTitle = substring(title, 0, indexOf(title, extension));

for(s = 1; s <= seriesCount; s++){
	newTitle = coreTitle + "_" + IJ.pad(s, lengthOf(toString(seriesCount))) + ".tif";
	if(!File.exists(dir + newTitle)){
		print("Series file " + newTitle + " doesn't exist, creating it...");
		options = "open=[" + path + "] autoscale color_mode=Composite rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_" + toString(s);
		run("Bio-Formats Importer", options);
		
		rename(newTitle);
		
		saveAs("tiff", dir + newTitle);
		close();
	}
}

setBatchMode(false);
beep();