# ND2Exporter

A simple ND2 Series exporter script. It automatically exports all series into the same directory as ND2 file by numbering them. Depending on the number of series, series number may be padded. e.g. if there are 10 series, single digit numbers will start with 0. With 100 series numbering would look like this:
001, 002, 003, .... 010, 011, .... 099, 100.
